/**
 * Defined includes
 */
const fs = require('fs');
const csvtojson = require('csv-parser')
const Handlebars = require('handlebars');


/**
 * Define necessary consts, such as the csv file to parse
 */
const args = process.argv.slice(2)
const datacsv = args[0]
//const datacsv = 'testfile.csv'
const base = fs.readFileSync('base-template.html').toString('utf8')

    /**
 * Use a file stream via csv-parser to create a JSON rip of the passed CSV data
 * Replacing the spaces in headers with - to prevent issues with object entries
 * @param csv - the path to the CSV file defined by our first argument
 * @returns {Promise} - containing the JSON data object parsed from the CSV
 */
async function getData(csv) {
    let jsdata = []
    return new Promise((resolve, reject) => {
    fs.createReadStream(csv).pipe(
        csvtojson({
            mapHeaders: ({header, index}) => header.replace((/ ||'/g), "").toLowerCase()
        })
            .on('data', (obj) => jsdata.push(obj))
            .on('end', () => resolve((jsdata)))
    )
})

}

/**
 * For moving array elements around conveniently
 * @param arr - the array to work on
 * @param fromIndex - the current index of the element
 * @param toIndex - the new desired index
 */
function arraymove(arr, fromIndex, toIndex) {
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}

/**
 * parse an array of objects and create subdivisions for these notes
 * @param a - an array of patch note objects containing labels
 * @returns {Promise} - promise containing re-sorted objects.
 */
async function subdivide(a) {
    let cats = {
        application: [],
        architecture: [],
        canvas: [],
        dice: [],
        documents: [],
        kb: [],
        other: []
    }
    for (let o of Object.values(a)) {
        let apptypes = ["Audio","AV","Chat","Combat Tracker","Controls","Macros","TinyMCE","UI","UX"]
        let arctypes = ["Authentication","Electron App","Folders and Organization","Packages","Safari","TD/OE","a11y"]
        let canvastypes = ["Canvas","Drawing Tools","Grid","Lighting/Fog","Templates","Tiles","Visual Effects","Walls"]
        let dicetypes = ["Dice"]
        let doctypes = ["Active Effects","Actors","Items","Journal","Scenes","Tokens","Roll Table","Compendium"]
        if (o.labels.some(l => apptypes.includes(l))) {cats.application.push(o); continue;}
        if (o.labels.some(l => arctypes.includes(l))) {cats.architecture.push(o); continue;}
        if (o.labels.some(l => canvastypes.includes(l))) {cats.canvas.push(o); continue;}
        if (o.labels.some(l => dicetypes.includes(l))) {cats.dice.push(o); continue;}
        if (o.labels.some(l => doctypes.includes(l))) {cats.documents.push(o); continue;}
        else {cats.other.push(o); continue;}
    }
    return cats;
}

/**
 * Using parsed json data from the gitlab CSV structure our patch notes data.
 * @param csv - the path to the CSV file defined by our first argument
 * @returns {Promise<void>}
 */
async function doTheThing(csv) {
    //Get our data object but automatically purge any confidentials
    let data = (await getData(datacsv)).filter(i => i.confidential !== 'Yes');
    //console.log(data);

    let breaking = []
    let bugs = []
    let features = []
    let api = []
    let i18n = []
    let documentation = []
    // prepare a bunch of data by reorganizing the labels
    for(let i of Object.values(data)) {
        //A minor string manipulation to make sure the last character in a title is always a period
        if (!i.title.endsWith('.')) {
            i.title = i.title+'.'
        }
        //convert the labels to an array
        i.labels = i.labels.split(",")
        //Repeal the cody labels
        if (i.labels.includes("Cody")) {
            let toRemove = i.labels.indexOf("Cody")
            i.labels.splice(toRemove,1)
        }
        //check if it's an API change and put API first in the array order for convenient sorting
        if (i.labels.includes("Bug")) {
            let toMove = i.labels.indexOf('Bug')
            if (toMove !== 0) {
                arraymove(i.labels,toMove,0)
            }
        }

        //check if it's a bug and put bug first in the array order for convenient sorting
        if (i.labels.includes("Bug")) {
            let toMove = i.labels.indexOf('Bug')
            if (toMove !== 0) {
                arraymove(i.labels,toMove,0)
              }
        }
        //check if it's localization related and put bug first in the array order for convenient sorting
        if (i.labels.includes("i18n")) {
            let toMove = i.labels.indexOf('i18n')
            if (toMove !== 0) {
                arraymove(i.labels,toMove,0)
            }
        }
        if (i.labels.includes("Documentation")) {
            let toMove = i.labels.indexOf('Documentation')
            if (toMove !== 0) {
                arraymove(i.labels,toMove,0)
            }
        }
        //Check if it's breaking and put breaking before anything, before bug even.
        if (i.labels.includes("Breaking")) {
            let toMove = i.labels.indexOf('Breaking')
            if (toMove !== 0) {
                arraymove(i.labels,toMove,0)
            }
        }

        let noteObj = {
            id: Number(i.issueid),
            url: i.url,
            labels: i.labels,
            title: i.title
        }
        switch(noteObj.labels[0]){
            case "Breaking": breaking.push(noteObj); break;
            case "Bug": bugs.push(noteObj); break;
            case "API": api.push(noteObj); break;
            case "i18n": i18n.push(noteObj); break;
            case "Documentation": documentation.push(noteObj); break;
            default: features.push(noteObj); break;
        }

/*
        if (noteObj.labels[0]("Breaking")) {
            breaking.push(noteObj)
        }
        if (noteObj.labels[0]("Bug")) {
            bugs.push(noteObj)
        }
        if (noteObj.labels[0]("API")) {
            api.push(noteObj)
        }
        if (!noteObj.labels.some(s => ["Breaking", "Bug", "API"].includes(s))) {
            features.push(noteObj)
        }*/
    }
    let breakingFinal = await subdivide(breaking)
    let bugsFinal = await subdivide(bugs)
    let featuresFinal = await subdivide(features)
    let apiFinal = await subdivide(api)
    const finalData = {
        breaking: breakingFinal,
        bugs: bugsFinal,
        features: featuresFinal,
        api: apiFinal,
        i18n: i18n,
        documentation: documentation
    }

    let template = Handlebars.compile(base)
    let output = template(finalData)
    let date = new Date()
    let d = date.toJSON().slice(2,10).replace(/-/g,'');
    let mmdd = d.substring(0,4).toString()
    let na = (d.slice(-2)-1).toString()
    d = mmdd+na
    //d = d.split(-2)
    let time = (date.getHours().toString())+((date.getMinutes()).toString())
    let outputfilename = "notes-"+d+"-"+time.toString()+".html"
    fs.writeFileSync(outputfilename, output, 'utf-8')
    console.log("Patch note preparation completed, file written to:" + outputfilename)
}

doTheThing(datacsv)