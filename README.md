a CSV to JSON parser which runs functions to clean up gitlab-exported CSVs and pre-format them for Foundry Virtual Tabletop patch note releases.

Usage
====
`node steno.js {exported-csv-file.csv}`

What it Does
======
The process used, in order:
- Strips confidential issues
- Repackages all CSV data as JSON
- Checks that all titles end with a period
- Sorts all issues into top level arrays, and sub-categorizes within them
- Passes the data into a handlebars template and renders the data into appropriate subcategories
- Appends the URL for each patch note entry as an `<a href='' target='_blank'>(id#)</a>` to the end of each item 
- Outputs the rendered html to a html file (`notes-{date}-{time}.html`)

This produces a 'ready to be re-written' patch note file, with the intent that each note be re-written for simplification and clarity. List items will need to be reorganized by priority within the subcategories. `<h4>` can be used within to denote specific themes.